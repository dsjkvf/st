#!/bin/bash

par='-w'

while true
do
    if [ $# -gt 0 ]
    then
        stonks "$@"
        exit 0
    else
        while true
        do
            printf "\033[1;36mTicker? \033[0m" && read -r st
            if [ $st = 'q' ] || [ $st = 'quit' ]
            then
                exit 0
            fi
            if [ $st = 'p' ] || [ $st = 'prefs' ]
            then
                printf "\033[1;36mChange parameters? \033[0m" && read -r par
                continue
            fi
            st=$(echo $st | tr 'a-z' 'A-Z')
            clear
            eval stonks $par $st
        done
    fi
done
