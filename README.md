st+ts
=====

## About

  - `st` is a simple wrapper script for [`stonks`](https://github.com/ericm/stonks).
  - `ts` is a simple wrapper script for [`tstocks`](https://github.com/Gbox4/tstock).
