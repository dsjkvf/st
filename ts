#!/bin/bash

par="-t=30min -y=$(( $(tput lines) - 9 ))"

while true
do
    # read the user input
    printf "\033[1;36mTicker? \033[0m"
    read st
    # clear the screen
    clear
    # check for options
    case $st in
        -h)
            tstock --help
            continue
            ;;
        -p)
            printf "\033[1;36mChange parameters? \033[0m" && read -r par
            par="$par -y=$(( $(tput lines) - 10 ))"
            continue
            ;;
        -q|q)
            exit 0
            ;;
    esac

    # get some basic information about a company
    IFS=$'\n' info=($(curl -s https://finviz.com/quote.ashx?t=$st | grep fullview-title -A 3 | sed 's/<[^>]*>//g' | perl -MHTML::Entities -pe 'decode_entities($_);'))
    # if the list of Revolut supported stocks is available, and if the ticker is not present there
    # show a warning
    if [ -f "$(dirname $(readlink $(which stocks-revolut-list)))/stocks-revolut-list.txt" ] && ! grep -iq "^$st$" "$(dirname $(readlink $(which stocks-revolut-list)))/stocks-revolut-list.txt"
    then
        printf "\033[1;30m%*s\033[0m\n" $(( (33 + $(tput cols)) / 2)) "WARNING: Not supported by REVOLUT"
    # otherwise, show an associated stock exchange
    else
        info[5]=${info[0]##*[}
        info[5]=${info[5]%%]*}
        printf "\033[1;30m%*s\033[0m\n" $(( (${#info[5]} + $(tput cols)) / 2)) ${info[5]}
    fi
    # and then print out the rest information about a company
    [ ${info[1]: -1} = "." ] && info[6]=${info[1]%?} || info[6]=${info[1]}
    printf "\033[1;30m%*s\033[0m\n" $(( (${#info[6]} + $(tput cols)) / 2)) ${info[6]}
    printf "\033[1;30m%*s\033[0m\n" $(( (${#info[2]} + $(tput cols)) / 2)) ${info[2]}

    # launch stonks
    eval tstock $par $st
done
